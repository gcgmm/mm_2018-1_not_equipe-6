﻿using UnityEngine;
using System.Collections;

public class CreatePositions : MonoBehaviour {

	public float[] rows = new float[6];
	public float[] cols = new float[6];

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Create()
	{
		for (int i = 0; i < rows.Length; ++i)
		{
			for (int j = 0; j < cols.Length; ++j)
			{
				GameObject obj = new GameObject ("Pos_" + i + "_" + j);
				obj.transform.Translate(new Vector3(rows[i], 0f, cols[j]));
				obj.transform.SetParent(this.transform,false);
			}
		}

	}
}
