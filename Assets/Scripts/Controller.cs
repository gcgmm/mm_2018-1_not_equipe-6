﻿using UnityEngine;
using System.Collections;

public class Controller : MonoBehaviour {

	/**
	 * Ordem dos Animais : 
	 * Vaca, Cachorro, Peixe, Rato, Porco
	 */

	GameObject[] cows;
	GameObject[] dogs;
	GameObject[] fishes;
	GameObject[] mice;
	GameObject[] pigs;

	Command[] commands;
	int currentCommand;

	void Start () {
		currentCommand = 0;
		cows = GameObject.FindGameObjectsWithTag("Cow");
		dogs = GameObject.FindGameObjectsWithTag("Dog");
		fishes = GameObject.FindGameObjectsWithTag("Fish");
		mice = GameObject.FindGameObjectsWithTag("Mouse");
		pigs = GameObject.FindGameObjectsWithTag("Pig");
		commands = GameObject.FindObjectsOfType<Command>();
		GameObject.FindObjectOfType<UIManagerScript>().SetFirstCommandText(GetCommandText());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	int CountVisibles(GameObject[] array)
	{
		int count = 0;
		for(int i = 0; i < array.Length; ++i)
		{
			if (array[i].GetComponent<MeshRenderer>().enabled)
				++count;
		}

		return count;
	}

	public bool CheckCommand()
	{
		int c = CountVisibles(cows);
		int d = CountVisibles(dogs);
		int f = CountVisibles(fishes);
		int m = CountVisibles(mice);
		int p = CountVisibles(pigs);

		bool sucesso = commands[currentCommand].CheckCommand(c, d, f, m, p);

		if (sucesso) {
		
			currentCommand++;

			if (currentCommand >= commands.Length) {
				//Debug.LogWarning ("Acabou!!");
				currentCommand = 0;
			}
		} else {
			//Debug.LogWarning ("Errou!!");
		}

		return sucesso;

	}

	public string GetCommandText()
	{
		return commands[currentCommand].GetCommandText();
	}

	public void UpdateAnimal()
	{
		PrintCurrentAnimals();
	}

	public void PrintCurrentAnimals()
	{
		Debug.LogWarning ("VACAS :" + CountVisibles(cows));
		Debug.LogWarning ("CÃES :" + CountVisibles(dogs));
		Debug.LogWarning ("PEIXES :" + CountVisibles(fishes));
		Debug.LogWarning ("RATOS :" + CountVisibles(mice));
		Debug.LogWarning ("PORCOS :" + CountVisibles(pigs)); 
	}
}