﻿using UnityEngine;
using System.Collections;

public class InvertVirtualCam : MonoBehaviour {

	Camera varCamera;
	public Vector3 v;

	void Start() {
		varCamera = GetComponent<Camera>();
	}

	void OnPreCull () {

		varCamera.ResetWorldToCameraMatrix ();
		varCamera.ResetProjectionMatrix ();

		Matrix4x4 p = varCamera.projectionMatrix;
		p = p * Matrix4x4.Scale(v);

		varCamera.projectionMatrix = p;

	}
	
	void OnPreRender () {
		GL.invertCulling = true;
	}
	
	void OnPostRender () {
		GL.invertCulling = false;
	}
}
