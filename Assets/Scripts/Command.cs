﻿using UnityEngine;
using System.Collections;

public class Command : MonoBehaviour {
	
	public int cows;
	public int dogs;
	public int fishes;
	public int mice;
	public int pigs;

	public bool CheckCommand(int c, int d,int f, int m, int p)
	{
		return (cows == c) && (dogs == d) && (fishes == f) && (mice == m) && (pigs == p);
	}

	public string GetCommandText()
	{
		string retorno = "";

		if (cows == 1)
			retorno += "1 VACA ";
		else if (cows > 1)
			retorno += cows + " VACAS ";

		if (dogs == 1)
			retorno += "1 CACHORRO ";
		else if (dogs > 1)
			retorno += dogs + " CACHORROS ";

		if (fishes == 1)
			retorno += "1 PEIXE ";
		else if (fishes > 1)
			retorno += fishes + " PEIXES ";
		
		if (mice == 1)
			retorno += "1 RATO ";
		else if (mice > 1)
			retorno += mice + " RATOS ";
		
		if (pigs == 1)
			retorno += "1 PORCO";
		else if (pigs > 1)
			retorno += pigs + " PORCOS";

		return retorno;
	}

}
