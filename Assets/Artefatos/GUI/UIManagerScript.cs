﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIManagerScript : MonoBehaviour {

	public Text commandText;
	public GameObject successText;
	public GameObject failedText;

	private Controller _controller;
	private Controller controller {
		get {
			if(_controller == null) {
				_controller = FindObjectOfType<Controller>();
			}
			return _controller;
		}
	}

	public void SetFirstCommandText(string text)
	{
		commandText.text = text;
	}

	public void EraseMessages()
	{
		successText.SetActive(false);
		failedText.SetActive(false);
	}

	public void CheckCommand()
	{
		EraseMessages ();

		bool success = controller.CheckCommand();
		if (success) {
			commandText.text = controller.GetCommandText();
			successText.SetActive(true);
		} else {
			failedText.SetActive(true);
		}
	}
}
