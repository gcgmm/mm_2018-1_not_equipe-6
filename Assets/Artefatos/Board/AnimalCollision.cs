﻿using UnityEngine;
using System.Collections;

public class AnimalCollision : MonoBehaviour {

	private MeshRenderer mesh;
	private Collider varCollider;
	private Collider piece;

	public bool isVisible;
	
	private Controller _controller;
	private Controller controller {
		get {
			if(_controller == null) {
				_controller = (Controller)FindObjectOfType(typeof(Controller));
				//^ this is the important line.
			}
			return _controller;
		}
	}
	
	// Use this for initialization
	void Start () {
		isVisible = false;

		mesh = GetComponent<MeshRenderer>();
		varCollider = GetComponent<Collider>();

		piece = null;
	}

	void Update()
	{
		if (varCollider.enabled)
		{
			if (piece == null)
			{
				MakeVisible(true);
			}
			else
			{
				MakeVisible(!piece.enabled);
				if (!piece.enabled)
					piece = null;
			}
		} else {
			piece = null;
			MakeVisible(false);
		}

		//atualizar com o mesh
		isVisible = mesh.enabled;
		
	}

	void MakeVisible(bool visible)
	{
		if (visible == isVisible)
			return;

		isVisible = visible;
		mesh.enabled = visible;
		controller.UpdateAnimal();

	}

	void OnTriggerStay(Collider other){
		piece = other;
		MakeVisible (false);
	}

	void OnTriggerExit(Collider other){
		piece = null;
		MakeVisible (true);
	}

}
